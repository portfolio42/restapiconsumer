package com.example.toremove.view.main

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.toremove.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*

private const val TAG = "MainActivity"

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val viewModel : MainViewModel by viewModels()

    private val recyclerAdapter : EmployeeListAdapter by lazy { EmployeeListAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupRecyclerView()
        setupSwipeRefresh()
        observeViewModel()
        viewModel.fetchData()
    }

    private fun setupSwipeRefresh() {
        employee_list_refresh_layout.setOnRefreshListener { viewModel.fetchData() }
    }

    private fun setupRecyclerView() {
        employee_list_recycler.apply {
            adapter = recyclerAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }

    private fun observeViewModel() {
        viewModel.employees.observe(this, { employees ->
            employees?.let { employeeList ->
                recyclerAdapter.submitList(employeeList)
            }
        })
    }

}