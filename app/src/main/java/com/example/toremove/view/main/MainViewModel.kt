package com.example.toremove.view.main

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.example.toremove.model.Employee
import com.example.toremove.network.EmployeesApi
import com.example.toremove.network.NetworkMapper
import com.example.toremove.network.responsemodel.EmployeesListEntity
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

private const val TAG = "MainViewModel"

class MainViewModel @ViewModelInject constructor(
    private val networkMapper: NetworkMapper,
    private val employeeApi: EmployeesApi
) : ViewModel() {

    private val _employees = MutableLiveData<List<Employee>>()

    val employees : LiveData<List<Employee>>
        get() = _employees

    fun fetchData() {
        viewModelScope.launch {
            employeeApi.getEmployees().enqueue(
                object : Callback<EmployeesListEntity> {
                    override fun onResponse(
                        call: Call<EmployeesListEntity>,
                        response: Response<EmployeesListEntity>
                    ) {
                        response.body()?.let { responseBody ->
                            val employeeList = networkMapper.mapFromEntityList(responseBody.data)
                            _employees.postValue(employeeList)
                        }
                    }

                    override fun onFailure(call: Call<EmployeesListEntity>, t: Throwable) {
                        Log.d(TAG, "onFailure")
                    }
                }
            )
        }

    }
}