package com.example.toremove.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val BASE_URL = "https://dummy.restapiexample.com/api/v1/"
object RetrofitProvider {

    private fun getRetrofit() : Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    fun employeesApi() : EmployeesApi {
        return getRetrofit().create(EmployeesApi::class.java)
    }
}