package com.example.toremove.network

import com.example.toremove.model.Employee
import com.example.toremove.network.responsemodel.EmployeeEntity
import com.example.toremove.util.EntityMapper

class NetworkMapper : EntityMapper<EmployeeEntity, Employee> {
    override fun mapToEntity(domainModel: Employee): EmployeeEntity {
        return EmployeeEntity(
            id = domainModel.id,
            employeeName = domainModel.employeeName,
            employeeSalary = domainModel.employeeSalary,
            employeeAge = domainModel.employeeAge,
            profileImage = domainModel.profileImage
        )
    }

    override fun mapFromEntity(entity: EmployeeEntity): Employee {
        return Employee(
            id = entity.id,
            employeeName = entity.employeeName,
            employeeSalary = entity.employeeSalary,
            employeeAge = entity.employeeAge,
            profileImage = entity.profileImage
        )
    }

    fun mapFromEntityList(entityList: List<EmployeeEntity>) : List<Employee> {
        return entityList.map(::mapFromEntity)
    }
}