package com.example.toremove.network

import com.example.toremove.network.responsemodel.EmployeesListEntity
import retrofit2.Call
import retrofit2.http.GET

interface EmployeesApi {

    @GET("employees")
    fun getEmployees() : Call<EmployeesListEntity>
}