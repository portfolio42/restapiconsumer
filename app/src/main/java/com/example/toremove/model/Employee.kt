package com.example.toremove.model

data class Employee (
    val id : Int,
    val employeeName : String,
    val employeeSalary : Int,
    val employeeAge : Int,
    val profileImage : String
)