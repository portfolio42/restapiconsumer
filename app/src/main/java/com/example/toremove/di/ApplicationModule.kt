package com.example.toremove.di

import com.example.toremove.network.BASE_URL
import com.example.toremove.network.EmployeesApi
import com.example.toremove.network.NetworkMapper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class ApplicationModule {

    @Singleton
    @Provides
    fun provideRetrofit() : Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Singleton
    @Provides
    fun provideEmployeesApi(retrofit: Retrofit) : EmployeesApi {
        return retrofit.create(EmployeesApi::class.java)
    }

    @Singleton
    @Provides
    fun provideNetworkMapper() : NetworkMapper{
        return NetworkMapper()
    }
}